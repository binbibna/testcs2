const URL ="https://63d767b9afbba6b7c93c6833.mockapi.io";



function renderSPList(sp) {

    var contentHTML = "";
  
    sp.reverse().forEach(function (item) {
      var contentTr =`
      <div class="card">
          <div class="top-bar">
              <i class="fab fa-apple"></i>
                  <em class="stocks">${item.type}</em>
          </div>
          <div class="img-container">
              <img class="product-img" src="${item.img}" alt="">
              <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
          </div>
          <div class="details">
              <div class="name-fav">
                  <strong class="product-name">${item.name}</strong>
                      <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i class="fas fa-heart"></i></button>
              </div>
              <div class="wrapper">
                  <h5>${item.desc}</h5>
                  <p>Camera sau : ${item.backCamera}</p>
                  <p>Camera trước : ${item.frontCamera}</p>
              </div>
              <div class="purchase">
                  <p class="product-price">$ ${item.price}</p>
                  <span class="btn-add">
                      <div>
                           <button id="${item.id}" onclick="addItem()" class="add-btn">Add <i class="fas fa-chevron-right"></i></button>
                       </div>
                  </span>
              </div>
          </div>
      </div>
    `
      contentHTML += contentTr;
    });
    document.getElementById("item").innerHTML = contentHTML;
  }


  



  // gọi api lấy danh sách món ăn từ server
function fetchSpList() {
    // bật loading 1 lần trước khi api được gọi
    // tắt loading 2 lần trong then và catch
    // batLoading();
    axios({
      url: `${URL}/Product`,
      method: "GET",
    })
      .then(function (res) {
        // tatLoading();
        var SPList = res.data;
        renderSPList(SPList);
      })
      .catch(function (err) {
        // tatLoading();
        console.log(`  🚀: err`, err);
      });
  }
  // khi user load trang
  fetchSpList();





function timkiem(obj){
  var ten = document.getElementById("txt-tim").value;
  function fetchSpList() {
    axios({
      url: `${URL}/Product`,
      method: "GET",
    })
      .then(function (res) {
        // tatLoading();
        var arr = [];
        var SPList = res.data;
        for(var index =0; index < SPList.length;index++){
          if(ten === SPList[index].type){
            arr.push(SPList[index]); 
          }
        }
        console.log(arr);
        var contentHTML = "";
  
    arr.reverse().forEach(function (item) {
      var contentTr =`
      <div class="card">
          <div class="top-bar">
              <i class="fab fa-apple"></i>
                  <em class="stocks">${item.type}</em>
          </div>
          <div class="img-container">
              <img class="product-img" src="${item.img}" alt="">
              <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
          </div>
          <div class="details">
              <div class="name-fav">
                  <strong class="product-name">${item.name}</strong>
                      <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i class="fas fa-heart"></i></button>
              </div>
              <div class="wrapper">
                  <h5>${item.desc}</h5>
                  <p>Camera sau : ${item.backCamera}</p>
                  <p>Camera trước : ${item.frontCamera}</p>
              </div>
              <div class="purchase">
                  <p class="product-price">$ ${item.price}</p>
                  <span class="btn-add">
                      <div>
                           <button id="${item.id}" onclick="addItem()" class="add-btn">Add <i class="fas fa-chevron-right"></i></button>
                       </div>
                  </span>
              </div>
          </div>
      </div>
    `
      contentHTML += contentTr;
    });
    document.getElementById("item").innerHTML = contentHTML;
      })
      .catch(function (err) {
        // tatLoading();
        console.log(`  🚀: err`, err);
      });
  }
  // khi user load trang
  fetchSpList();
}



